# COMPUTER GRAPHICS #

## 4 branch ##
- **progetto01**: Scena animata 2D che fa uso delle librerie OpegnGL
- **progetto02**: Scena animata 3D che fa uso delle librerie OpegnGL
- **progetto03**: Editor Grafico per la Costruzione di Curve (Bezier, Spline, Spline a Nodi Multipli, Nurbs)
- **progetto04**: Modellazione e Rendering di una scena 3D facendo uso di Blender (realizzare un video ottenuto facendo il rendering delle immagini prodotte da una telecamera che si sposta nella scena).